import {Directive, ElementRef, OnInit} from '@angular/core';

@Directive({
  selector: '[appInputFocus]'
})
export class FocusInputDirective implements OnInit {

  constructor(private elRef: ElementRef) {}

  ngOnInit() {
    this.elRef.nativeElement.focus();
  }
}
