import {Directive, OnInit, OnDestroy, Output, EventEmitter, ElementRef} from '@angular/core';
import {Subscription, fromEvent} from 'rxjs';
import { delay, tap } from 'rxjs/operators';

@Directive({
  selector: '[clickNotChild]'
})

export class ClickNotChildDirective implements OnInit, OnDestroy {
  private listening: boolean;
  private elementClick: Subscription;

  @Output('clickNotChild') clickNotChild: EventEmitter<object>;

  constructor(private elRef: ElementRef) {
    this.listening = false;
    this.clickNotChild = new EventEmitter();
  }

  ngOnInit() {
    this.elementClick = fromEvent(this.elRef.nativeElement, 'click')
      .pipe(
        tap(() => {
          this.listening = true;
        })
      ).subscribe((event: MouseEvent) => {
        this.onElementClick(event);
      });
  }

  ngOnDestroy() {
    this.elementClick.unsubscribe();
  }

  onElementClick(event: MouseEvent) {
    if (event instanceof MouseEvent
      && this.listening === true
      && this.elRef.nativeElement === event.target) {

      this.clickNotChild.emit({
        target: (event.target || null),
        value: true
      });
    }
  }
}
