import {Directive, OnInit, OnDestroy, Output, EventEmitter, ElementRef} from '@angular/core';
import {Subscription, fromEvent} from 'rxjs';
import { delay, tap } from 'rxjs/operators';

@Directive({
  selector: '[clickOutside]'
})

export class ClickOutsideDirective implements OnInit, OnDestroy {
  private listening: boolean;
  private globalClick: Subscription;

  @Output('clickOutside') clickOutside: EventEmitter<object>;

  constructor(private elRef: ElementRef) {
    this.listening = false;
    this.clickOutside = new EventEmitter();
  }

  ngOnInit() {
    this.globalClick = fromEvent(document, 'click')
      .pipe(
        tap(() => {
          this.listening = true;
        })
      ).subscribe((event: MouseEvent) => {
        this.onGlobalClick(event);
      });
  }

  ngOnDestroy() {
    this.globalClick.unsubscribe();
  }

  onGlobalClick(event: MouseEvent) {
    if (event instanceof MouseEvent
      && this.listening === true
      && this.isDescendant(this.elRef.nativeElement, event.target) === false) {

      this.clickOutside.emit({
        target: (event.target || null),
        value: true
      });
    }
  }

  isDescendant(parent, child) {
    let node = child;
    while (node !== null) {
      if (node === parent) {
        return true;
      } else {
        node = node.parentNode;
      }
    }
    return false;
  }
}
