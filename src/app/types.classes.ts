export class Payload {
  id: string;
  iat: number;
  exp?: number;
}

