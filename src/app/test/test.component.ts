import { Component, OnInit } from '@angular/core';
import {SocketService} from '../services/socket.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(private socketService: SocketService) {}

  ngOnInit() {

    this.socketService.connect();
  }

  getData() {

    this.socketService.socket.emit('get_data', {}, (response) => {
      console.log(JSON.parse(response));
    });
  }

  join() {

    this.socketService.socket.emit('join_room', {}, (response) => {
      console.log(response);
    });
  }

  leave() {

    this.socketService.socket.emit('leave_room', {}, (response) => {
      console.log(response);
    });
  }
}
