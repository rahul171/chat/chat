import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {GroupsComponent} from '../main/groups/groups.component';
import {ProfileComponent} from '../main/profile/profile.component';
import {LoginComponent} from '../auth/login/login.component';
import {SignupComponent} from '../auth/signup/signup.component';
import {LogoutComponent} from '../auth/logout/logout.component';
import {MainComponent} from '../main/main.component';
import {PrivateChatComponent} from '../main/private-chat/private-chat.component';
import {AuthGuardService} from '../services/auth-guard.service';
import {TestComponent} from '../test/test.component';
import {GroupsCanDeactivate} from '../interfaces/groups-can-deactivate';

const routes = [
  {
    path: '',
    redirectTo: '/private',
    pathMatch: 'full'
  },
  {
    path: 'private',
    canActivate: [AuthGuardService],
    component: PrivateChatComponent,
  },
  {
    path: 'groups',
    children: [
      {
        path: '',
        component: GroupsComponent,
        canActivate: [AuthGuardService],
      },
      {
        path: ':id',
        component: GroupsComponent,
        canActivate: [AuthGuardService],
        canDeactivate: [AuthGuardService]
      }
    ]
  },
  {
    path: 'profile/:id',
    // canActivate: [AuthGuardService],
    component: ProfileComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'test',
    component: TestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
