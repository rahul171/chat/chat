import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, CanDeactivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';
import {SocketService} from './socket.service';
import {GetNameService} from '../get-name.service';
import {GroupsCanDeactivate} from '../interfaces/groups-can-deactivate';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanDeactivate<GroupsCanDeactivate> {

  constructor(private authService: AuthService,
              private router: Router,
              private socketService: SocketService,
              private getNameService: GetNameService) {}

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.getNameService.logName();

    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/login']);
      console.log('not authenticated: (AuthGuardService -> canActivate())');
      return false;
    }

    if (!this.socketService.socket) {
      console.log('AuthGuardService -> canActivate() => socket not connected');
      this.socketService.connect();
      console.log('AuthGuardService -> canActivate() => sending connect request');
    }

    console.log('leaving from => ' + this.getNameService.logName());

    return true;
  }

  canDeactivate(component: GroupsCanDeactivate,
                route: ActivatedRouteSnapshot,
                state: RouterStateSnapshot,
                nextState?: RouterStateSnapshot) {

    this.getNameService.logName();

    return component.canDeactivate();
  }
}
