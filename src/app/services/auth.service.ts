import {Injectable, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: string;
  username: string;
  userId: string;

  constructor(private http: HttpClient, private router: Router) {}

  isAuthenticated(): boolean {
    const token = this.getToken();

    if (!token) {
      return false;
    }

    const payload = this.getPayload(token);

    if (payload.exp && parseInt(payload.exp, 10) < Math.floor(Date.now() / 1000)) {
      this.removeToken();
      return false;
    }

    return true;
  }

  getPayload(token = this.getToken()): any {

    let payload = token.split('.')[1];

    if (typeof window !== 'undefined') {
      if (typeof window.atob !== 'undefined') {
        payload = window.atob(payload);
      }
    } else {
      // @ts-ignore
      payload = new Buffer(payload, 'base64').toString('utf-8');
    }

    payload = JSON.parse(payload);

    return payload;
  }

  getUserId() {
    return this.userId = this.userId || this.getPayload().id;
  }

  getUsername() {
    return this.username = this.username || this.getPayload().username;
  }

  login(username: string, password: string) {
    this.username = username;

    return this.http.post(environment.api.url + '/login', {
        username, password
      }).pipe(
        tap((data: any) => {
          this.setToken(data.token);
        })
      );
  }

  signup() {}

  logout() {
    this.removeToken();
    this.router.navigate(['/login']);
  }

  setToken(token: string) {
    this.token = token;
    localStorage.setItem('token', token);
  }

  getToken(): string {
    this.token = this.token || localStorage.getItem('token');
    // return this.token;
    return localStorage.getItem('token');
  }

  removeToken() {
    this.token = '';
    localStorage.removeItem('token');
  }
}
