import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';

declare const io: any;

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket: any;

  constructor(private authService: AuthService, private router: Router) {}

  connect() {
    const token = this.authService.getToken();

    this.socket = io('http://localhost:4000', {
      query: {
        token,
        username: this.authService.username
      },
      autoConnect: false
    });

    this.errorHandler();
    this.socket.open();
  }

  errorHandler() {

    this.socket.on('error', (err) => {
      console.log('SocketService -> errorHandler()');
      console.log(err);
      this.removeSocket();
      this.authService.logout();
    });
  }

  removeSocket() {
    this.socket.close();
    this.socket = '';
  }

  isConnected(): boolean {
    return this.socket.connected;
  }
}
