import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  clone(obj) {
    if (obj === null || typeof (obj) !== 'object' || 'isActiveClone' in obj) {
      return obj;
    }

    let temp;

    if (obj instanceof Date) {
      // @ts-ignore
      temp = new obj.constructor(); // or new Date(obj);
    } else {
      temp = obj.constructor();
    }

    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        obj.isActiveClone = null;
        temp[key] = this.clone(obj[key]);
        delete obj.isActiveClone;
      }
    }
    return temp;
  }
}
