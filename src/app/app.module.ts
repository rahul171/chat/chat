import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './modules/app-routing.module';
import {LogoutComponent} from './auth/logout/logout.component';
import {SignupComponent} from './auth/signup/signup.component';
import {LoginComponent} from './auth/login/login.component';
import {ProfileComponent} from './main/profile/profile.component';
import {GroupsComponent} from './main/groups/groups.component';
import {PrivateChatComponent} from './main/private-chat/private-chat.component';
import {AuthComponent} from './auth/auth.component';
import {MainComponent} from './main/main.component';
import {HeaderComponent} from './header/header.component';
import { TestComponent } from './test/test.component';
// import {ClickOutsideModule} from 'ng-click-outside';
import {ClickOutsideDirective} from './directives/click-outside.directive';
import {ClickNotChildDirective} from './directives/click-not-child.directive';
import {FocusInputDirective} from './directives/focus-input.directive';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    AuthComponent,
    PrivateChatComponent,
    GroupsComponent,
    ProfileComponent,
    LoginComponent,
    SignupComponent,
    LogoutComponent,
    TestComponent,
    ClickOutsideDirective,
    ClickNotChildDirective,
    FocusInputDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
