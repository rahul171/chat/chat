import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  header = [
    [
      {
        name: 'Private',
        url: '/private'
      },
      {
        name: 'Groups',
        url: '/groups'
      },
      {
        name: 'Profile',
        url: '/profile'
      }
    ],
    [
      {
        name: 'Login',
        url: '/login'
      },
      {
        name: 'SignUp',
        url: '/signup'
      },
      {
        name: 'Logout',
        url: '/logout'
      }
    ]
  ];

  constructor() {}

  ngOnInit() {}
}
