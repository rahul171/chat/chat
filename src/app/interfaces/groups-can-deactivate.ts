import {Observable} from 'rxjs';

export interface GroupsCanDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}
