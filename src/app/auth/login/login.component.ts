import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {SocketService} from '../../services/socket.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss', '../form.style.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  serverError = '';

  constructor(private http: HttpClient,
              private router: Router,
              private authService: AuthService,
              private socketService: SocketService) {}

  ngOnInit() {

    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  submit() {
    this.serverError = '';

    if (this.form.invalid) {
      return;
    }

    const values = this.form.value;

    this.authService.login(values.username, values.password)
      .subscribe(data => {

        this.socketService.connect();
        this.router.navigate(['/groups']);
      }, err => {
        this.serverError = err.error.message;
      });
  }
}
