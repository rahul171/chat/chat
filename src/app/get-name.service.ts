import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GetNameService {

  getName(i = 3) {
    let a = new Error();

    let b = ((a.stack.split('at ')[i].match(/(^|\.| <| )(.*[^(<])( \()/) || [])[2] || '').trim();

    b = b.replace('.', ' -> ');

    return b;
  }

  logName() {
    const name = this.getName();
    console.log(name);
    return name;
  }

  getNameBKP(d = 1) {
    const error = new Error();
    const firefoxMatch = (error.stack.split('\n')[0 + d].match(/^.*(?=@)/) || [])[0];
    const chromeMatch = ((((error.stack.split('at ') || [])[1 + d] || '').match(/(^|\.| <| )(.*[^(<])( \()/) || [])[2] || '').split('.').pop();
    const safariMatch = error.stack.split('\n')[0 + d];

    // firefoxMatch ? console.log('firefoxMatch', firefoxMatch) : void 0;
    // chromeMatch ? console.log('chromeMatch', chromeMatch) : void 0;
    // safariMatch ? console.log('safariMatch', safariMatch) : void 0;

    return firefoxMatch || chromeMatch || safariMatch;
  }
}
