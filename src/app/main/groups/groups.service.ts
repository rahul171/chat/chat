import {Injectable, OnInit} from '@angular/core';
import {SocketService} from '../../services/socket.service';
import {AuthService} from '../../services/auth.service';
import {GetNameService} from '../../get-name.service';
import {BehaviorSubject, Subject} from 'rxjs';
import {UtilityService} from '../../services/utility.service';

@Injectable({
  providedIn: 'root'
})
export class GroupsService {
  groups = [];
  groupId: string;
  selectedGroupIndex: number;
  listenGroupId: string;
  handlersSet = false;
  groupsSet = false;

  groupsSubject = new BehaviorSubject<any>([]);
  selectedGroupIndexSubject = new BehaviorSubject<any>(null);

  constructor(private socketService: SocketService,
              private authService: AuthService,
              private getNameService: GetNameService,
              private utility: UtilityService) {}

  isGroupJoined(id = this.getGroupId()) {
    this.getNameService.logName();

    const group = this.groups.find((item) => {
      return item.id === id;
    });

    return group && group.joined;
  }

  getGroupId() {
    this.getNameService.logName();
    return this.groupId;
  }

  setGroup(id: string) {
    this.getNameService.logName();
    this.groupId = id;
    this.setSelectedGroupIndex(id);
  }

  setSelectedGroupIndex(id) {
    this.getNameService.logName();
    this.selectedGroupIndex = this.groups.findIndex(group => {
      return group.id === id;
    });
    this.selectedGroupIndexSubject.next(this.selectedGroupIndex);
  }

  setGroups(groups) {
    this.getNameService.logName();
    this.groups = groups;
    this.emitGroups();
  }

  addGroup(group) {
    this.getNameService.logName();
    this.groups.push(group);
    this.emitGroups();
  }

  removeGroup(id) {
    this.getNameService.logName();
    const index = this.groups.findIndex(group => group.id === id);

    if (index === -1) {
      return;
    }

    this.groups.splice(index, 1);
    this.emitGroups();
  }

  addGroupMember(id, userId) {
    this.getNameService.logName();

    const index = this.groups.findIndex(group => group.id === id);

    if (index === -1) {
      return;
    }

    this.groups[index].members.push(userId);

    // temporary, make this better
    this.groups[index].joined = true;

    this.emitGroups();
  }

  removeGroupMember(id, userId) {
    this.getNameService.logName();
    const index = this.groups.findIndex(group => group.id === id);
    const memberIndex = this.groups[index].members.findIndex(user => user === userId);

    if (index === -1) {
      return;
    }

    this.groups[index].members.splice(memberIndex, 1);

    // temporary, make this better
    this.groups[index].joined = false;

    this.emitGroups();
  }

  emitGroups() {
    this.getNameService.logName();
    this.groupsSubject.next(this.groups);
  }

  getListenGroupId() {
    this.getNameService.logName();
    return this.listenGroupId;
  }

  setListenGroupId(id) {
    this.getNameService.logName();
    this.listenGroupId = id;
  }

  setInitialGroups() {
    this.getNameService.logName();
    return this.getGroups()
      .then((res: any) => {
        this.configureGroups(res.groups);

        console.log(res);
      })
      .catch(err => {
        this.error(err);
        throw err;
      });
  }

  configureGroups(groups) {
    const clientGroups = this.utility.clone(groups);

    this.setTimeProperties(clientGroups);
    this.sortByJoined(clientGroups);
    this.setGroups(clientGroups);
    this.groupsSet = true;

    return clientGroups;
  }

  setTimeProperties(groups) {
    groups.forEach(group => {
      group.messages.forEach(message => {
        this.setMessageTimeProperties(message);
      });
    });
  }

  to2Digit(time) {
    return ('0' + time).slice(-2);
  }

  sortByJoined(groups) {
    groups.sort((a, b) => {
      return a.joined === b.joined ? 0 : (a.joined ? -1 : 1);
    });
  }

  getGroups() {
    this.getNameService.logName();
    return new Promise((resolve, reject) => {
      this.socketService.socket
        .emit('get_groups', {
          token: this.authService.getToken()
        }, (response) => {
          if (response.err) {
            reject(response.err);
          }
          resolve(response);
        });
    });
  }

  areGroupsSet() {
    this.getNameService.logName();
    return this.groupsSet;
  }

  setHandlers() {
    this.getNameService.logName();
    this.setIncomingGroupMessageHandler();
    this.setReconnectHandler();
    this.handlersSet = true;
  }

  setIncomingGroupMessageHandler() {
    this.getNameService.logName();
    this.socketService.socket.on('group_message', (data) => {
      console.log('socket - group_message');
      console.log(data);
      this.addGroupMessage(data);
    });
  }

  addGroupMessage(message) {
    this.getNameService.logName();
    this.setMessageTimeProperties(message);
    this.groups[this.selectedGroupIndex].messages.push(message);
    this.emitGroups();
  }

  setMessageTimeProperties(message) {
    const d = new Date(message.time);
    let hours = d.getHours();

    message.time = {};
    message.time.ampm = hours > 12 ? 'pm' : 'am';

    hours %= 12;
    hours = hours ? hours : 12; // 0 should be 12
    message.time.hours = this.to2Digit(hours);

    message.time.minutes = this.to2Digit(d.getMinutes());
    message.time.seconds = this.to2Digit(d.getSeconds());

    message.time.date = d.toLocaleDateString();
  }

  setReconnectHandler() {
    this.getNameService.logName();
    this.socketService.socket.on('reconnect', (data) => {
      console.log('socket - reconnect');

      const id = this.getListenGroupId();

      if (id) {
        this.listenGroup(id);
      }
    });
  }

  areHandlersSet() {
    this.getNameService.logName();
    return this.handlersSet;
  }

  removeHandlers() {
    this.getNameService.logName();
    this.removeIncomingMessageHandler();
    this.removeReconnectHandler();
    this.handlersSet = false;
  }

  removeIncomingMessageHandler() {
    this.getNameService.logName();
    this.socketService.socket.off('group_message');
  }

  removeReconnectHandler() {
    this.getNameService.logName();
    this.socketService.socket.off('reconnect');
  }

  createGroup(name: string) {
    this.getNameService.logName();

    return this.create(name)
      .then((res: any) => {
        const group = res.group;
        this.addGroup(group);
        console.log(res);
        return group;
      })
      .catch(err => {
        this.error(err);
        throw err;
      });
  }

  create(name: string) {
    this.getNameService.logName();
    return new Promise((resolve, reject) => {
      this.socketService.socket
        .emit('create_group', {
          name,
          token: this.authService.getToken()
        }, (response) => {
          if (response.err) {
            reject(response.err);
          }
          resolve(response);
        });
    });
  }

  joinGroup(id = this.getGroupId()) {
    this.getNameService.logName();

    return this.join(id)
      .then((res: any) => {
        this.addGroupMember(id, this.authService.getUserId());
        this.setListenGroupId(id);
        console.log(res);
        return res;
      })
      .catch(err => {
        this.error(err);
        throw err;
      });
  }

  join(id) {
    this.getNameService.logName();
    return new Promise((resolve, reject) => {
      this.socketService.socket
        .emit('join_group', {
          id,
          token: this.authService.getToken()
        }, (response) => {
          if (response.err) {
            reject(response.err);
          }
          resolve(response);
        });
    });
  }

  listenGroup(id = this.getGroupId()) {
    this.getNameService.logName();
    console.log('send listen request: ' + id);

    return this.listen(id)
      .then((res) => {
        // send group id from server so that we can set `this.listenGroupId` to it
        // right now we'll do it as below.
        this.setListenGroupId(id);
        console.log(res);
        return res;
      })
      .catch(err => {
        this.error(err);
        throw err;
      });
  }

  listen(id) {
    this.getNameService.logName();
    return new Promise((resolve, reject) => {
      this.socketService.socket.emit('listen_to_group', {
        id,
        token: this.authService.getToken()
      }, (response) => {
        if (response.err) {
          reject(response.err);
        }
        resolve(response);
      });
    });
  }

  stopListenGroup(id = this.getListenGroupId()) {
    this.getNameService.logName();
    console.log('sending stop listening request: ' + id);

    return this.stopListen(id)
      .then((res) => {
        this.setListenGroupId('');
        console.log(res);
        return res;
      })
      .catch(err => {
        this.error(err);
        throw err;
      });
  }

  stopListen(id) {
    this.getNameService.logName();
    return new Promise((resolve, reject) => {
      this.socketService.socket.emit('stop_listen_to_group', {
        id,
        token: this.authService.getToken()
      }, (response) => {
        if (response.err) {
          reject(response.err);
        }
        resolve(response);
      });
    });
  }

  sendGroupMessage(message: string, id = this.getListenGroupId()) {
    this.getNameService.logName();

    console.log('listen group_id: ' + id);

    return this.send(message, id)
      .then((res: any) => {
        console.log(res);
        this.addGroupMessage(res.messageData);
        return res;
      })
      .catch(err => {
        this.error(err);
        throw err;
      });
  }

  send(message: string, id) {
    this.getNameService.logName();
    return new Promise((resolve, reject) => {
      this.socketService.socket.emit('send_to_group', {
        id,
        message,
        token: this.authService.getToken()
      }, (response) => {
        if (response.err) {
          reject(response.err);
        }
        resolve(response);
      });
    });
  }

  leaveGroup(id = this.getGroupId()) {
    this.getNameService.logName();
    return this.leave(id)
      .then((res: any) => {
        this.setListenGroupId('');

        if (res.deleted) {
          this.removeGroup(id);
        } else {
          this.removeGroupMember(id, this.authService.getUserId());
        }

        console.log(res);
        return res;
      })
      .catch(err => {
        this.error(err);
        throw err;
      });
  }

  leave(id) {
    this.getNameService.logName();
    // this.group = {};
    return new Promise((resolve, reject) => {
      this.socketService.socket
        .emit('leave_group', {
          id,
          token: this.authService.getToken()
        }, (response) => {
          if (response.err) {
            reject(response.err);
          }
          resolve(response);
        });
    });
  }

  removeListening() {
    this.getNameService.logName();
    const id = this.getListenGroupId();

    if (id) {
      this.stopListenGroup(id)
        .then(data => {
          console.log(data);
        })
        .catch(err => {
          this.error(err);
        });
    }
  }

  error(err) {
    this.getNameService.logName();
    console.log(err);
  }
}
