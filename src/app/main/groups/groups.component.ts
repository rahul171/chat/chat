import {AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {SocketService} from '../../services/socket.service';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {GroupsService} from './groups.service';
import {Subscription} from 'rxjs';
import {GetNameService} from '../../get-name.service';
import {GroupsCanDeactivate} from '../../interfaces/groups-can-deactivate';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit, OnDestroy, GroupsCanDeactivate {
  groups = [];
  selectedGroupIndex: number;
  @ViewChild('message', {static: false}) message;
  @ViewChild('messageList', {static: false}) messageList;
  userId: string;
  createGroupForm: FormGroup;
  createGroupErrorMessage = '';

  dynamicElements = {
    groupsDropdown: false,
    dataDropdown: false,
    createGroupPopup: false
  };

  groupsSubscription: Subscription;
  indexSubscription: Subscription;

  constructor(private socketService: SocketService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute,
              private groupsService: GroupsService,
              private getNameService: GetNameService,
              private renderer: Renderer2,
              private cdr: ChangeDetectorRef) {}

  ngOnInit() {

    this.userId = this.authService.getUserId();

    console.log('***ngOnInit***');

    this.setSubscriptions();

    this.route.params.subscribe((params) => {

      const promise = new Promise((resolve, reject) => {

        console.log('are groups set: ' + this.groupsService.areGroupsSet());

        if (this.groupsService.areGroupsSet()) {
          resolve();
          return;
        }

        this.groupsService.setInitialGroups()
          .then(() => {
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });

      promise.then(() => {

        const groupId = params.id;

        if (groupId) {
          console.log('are handlers set: ' + this.groupsService.areHandlersSet());

          if (!this.groupsService.areHandlersSet()) {
            this.groupsService.setHandlers();
          }

          this.groupsService.setGroup(groupId);

          console.log('is group joined: ' + this.groupsService.isGroupJoined(groupId));

          if (this.groupsService.isGroupJoined(groupId)) {
            this.groupsService.listenGroup(groupId)
              .catch(err => {
                this.error(err);
              });
          } else {
            // this.groupsService.setListenGroupId('');
          }
        }
      }).catch(err => {
        this.error(err);
      });
    });
  }

  setSubscriptions() {
    this.getNameService.logName();
    this.setGroupsSubscription();
    this.setIndexSubscription();
  }

  removeSubscriptions() {
    this.getNameService.logName();
    this.removeGroupsSubscription();
    this.removeIndexSubscription();
  }

  setGroupsSubscription() {
    this.getNameService.logName();
    this.groupsSubscription = this.groupsService.groupsSubject.subscribe((groups) => {
      console.log('--- got groups subject');
      this.groups = groups;
      // if(groups.length > 0) console.log(new Date(groups[0].messages[groups[0].messages.length - 1].time));
      console.log(groups);
    });
  }

  removeGroupsSubscription() {
    this.getNameService.logName();
    this.groupsSubscription.unsubscribe();
  }

  setIndexSubscription() {
    this.getNameService.logName();
    this.indexSubscription = this.groupsService.selectedGroupIndexSubject.subscribe((index) => {
      console.log('--- got selected index subject');
      this.selectedGroupIndex = index;
      console.log(index);
    });
  }

  removeIndexSubscription() {
    this.getNameService.logName();
    this.indexSubscription.unsubscribe();
  }

  listenGroup() {
    this.getNameService.logName();

    this.groupsService.listenGroup()
      .then(data => {
        console.log(data);
      })
      .catch(err => {
        this.error(err);
      });
  }

  stopListenGroup() {
    this.getNameService.logName();
    this.groupsService.stopListenGroup()
      .then(data => {
        console.log(data);
      })
      .catch(err => {
        this.error(err);
      });
  }

  joinGroup() {
    this.getNameService.logName();

    this.groupsService.joinGroup()
      .then(data => {
        console.log(data);
      })
      .catch(err => {
        this.error(err);
      });

    this.closeDataDropdown();
  }

  createGroup() {
    this.getNameService.logName();

    const name = this.createGroupForm.value.name;

    if (!name) {
      return;
    }

    this.groupsService.createGroup(name)
      .then(group => {
        console.log(group);
        this.router.navigate(['/groups', group.id]);

        // no need to close popup if you are redirecting from groups or groups/:id using router
        // (because component is going to initialize again)
        // but when redirecting from groups/:id to groups/:id, we need to close it
        this.closeCreateGroupPopup();
      })
      .catch(err => {
        this.createGroupErrorMessage = err.message;
        this.error(err);
      });
  }

  sendGroupMessage(event) {
    if (event) {
      // to prevent adding newline in send-message-input when user hit enter
      event.preventDefault();
    }

    this.getNameService.logName();
    const message = this.message.nativeElement.innerText.trim();

    if (!message) {
      return;
    }

    this.groupsService.sendGroupMessage(message)
      .then(res => {
        console.log(res);

        // without this, automatic change detection will not run immediately and
        // we'll get scrollHeight without the height of new element added
        this.cdr.detectChanges();

        this.messageList.nativeElement.scrollTop = this.messageList.nativeElement.scrollHeight;
      })
      .catch(err => {
        this.error(err);
      });

    this.clearMessageInput();
  }

  clearMessageInput() {
    this.message.nativeElement.innerText = '';
  }

  leaveGroup() {
    this.getNameService.logName();
    this.groupsService.leaveGroup()
      .then(res => {
        console.log(res);
        if (res.deleted) {
          this.router.navigate(['/groups']);
        }
      })
      .catch(err => {
        this.error(err);
      });

    // no need to close dropdown if you are redirecting from groups/:id to /groups using router
    // (because component is going to initialize again)
    // but still, close it in case due to some changes in future, component wont initialize again
    this.closeDataDropdown();
  }

  toggleGroupsDropdown() {
    this.dynamicElements.groupsDropdown = !this.dynamicElements.groupsDropdown;
  }

  closeGroupsDropdown() {
    this.dynamicElements.groupsDropdown = false;
  }

  toggleDataDropdown() {
    this.dynamicElements.dataDropdown = !this.dynamicElements.dataDropdown;
  }

  closeDataDropdown() {
    this.dynamicElements.dataDropdown = false;
  }

  showCreateGroupPopup() {
    this.closeGroupsDropdown();

    if (!this.createGroupForm) {
      this.createGroupForm = new FormGroup({
        name: new FormControl()
      });
    }

    this.resetCreateGroupPopup();
    this.dynamicElements.createGroupPopup = true;
  }

  resetCreateGroupPopup() {
    this.createGroupErrorMessage = '';
    this.createGroupForm.get('name').setValue('');
  }

  closeCreateGroupPopup() {
    this.dynamicElements.createGroupPopup = false;
  }

  stopPropagation(event) {
    event.stopPropagation();
  }

  error(err) {
    this.getNameService.logName();
    console.log(err);
    if (err.code === 1) {
      this.router.navigate(['/login']);
    }
  }

  ngOnDestroy() {
    this.getNameService.logName();
    // this.removeSocketListeners();
    this.groupsService.removeHandlers();
    this.groupsService.removeListening();
    this.removeSubscriptions();
    console.log('destroyed');
  }

  canDeactivate() {
    this.getNameService.logName();
    this.groupsService.removeListening();
    return true;
  }
}
